import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const FieldContainer = styled.div`
  flex-basis: ${(props) => 100 / props.level + '%'};
  padding: 1px;
  background: #376461;
`;

const Field = styled.div`
  position: relative;
  height: 0;
  padding-top: 100%;
  color: #1c3c40;
  background: #fff;
  cursor: pointer;
  transition: 0.3s;

  &:hover {
    opacity: ${(props) => (props.active ? 0.75 : 1)};
  }

  &:active {
    color: #1b937d;
  }
`;

const FieldValue = styled.span`
  display: inline-block;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  font-size: ${(props) => props.fSize + 'px'};
  pointer-events: none;
`;

export default function Cell(props) {
  const { value, level, active, fontSize, onClick } = props;

  const handleClick = (e) => {
    const fieldValue = Number(e.target.dataset.testid);
    onClick(fieldValue);
  };

  return (
    <FieldContainer level={level}>
      <Field
        onClick={handleClick}
        active={active}
        data-testid={value}
      >
        <FieldValue fSize={fontSize}>{value}</FieldValue>
      </Field>
    </FieldContainer>
  );
}

Cell.propTypes = {
  value: PropTypes.number,
  level: PropTypes.number,
  active: PropTypes.bool,
  fontSize: PropTypes.number,
  onClick: PropTypes.func,
};
