import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

const Info = styled.div`
  text-align: center;
  font-size: 200px;
  font-weight: bold;
  text-shadow: 0 4px 4px #1b1210;
  color: ${(props) => (props.error ? '#D76001' : '#1C3C40')};

  @media (max-width: 1600px) {
    font-size: 170px;
  }

  @media (max-width: 1200px) {
    font-size: 140px;
  }

  @media (max-width: 768px) {
    font-size: 32px;
  }
`;

function ConnectedInfo({ nextValue, error }) {
  return <Info error={error}>{nextValue}</Info>;
}

function mapStateToProps(state) {
  return {
    nextValue: state.nextValue,
    error: state.error,
  };
}

export default connect(mapStateToProps)(ConnectedInfo);
