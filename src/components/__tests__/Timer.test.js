import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

import Timer from '../Timer';

describe('Timer component', () => {
  const props = {
    enabled: false,
    time: 0,
    onClick: jest.fn(),
  };

  test('renders correctly', () => {
    const { container } = render(<Timer {...props} />);

    expect(container.firstChild).toMatchSnapshot();
    expect(screen.getByText(/0:00\.0/)).toBeInTheDocument();
    expect(screen.getByText(/СТОП/)).toBeInTheDocument();
    //screen.debug();
  });

  test('renders before game starting', () => {
    render(<Timer {...props} />);

    expect(screen.getByText(/СТОП/)).toBeDisabled();
    fireEvent.click(screen.getByText(/СТОП/));
    expect(props.onClick).toHaveBeenCalledTimes(0);
  });

  test('renders after game starting', () => {
    const newProps = { ...props, enabled: true };
    render(<Timer {...newProps} />);

    expect(screen.getByText(/СТОП/)).toBeEnabled();
    fireEvent.click(screen.getByText(/СТОП/));
    expect(newProps.onClick).toHaveBeenCalledTimes(1);
  });
});
