import React from 'react';
import { render, screen } from '@testing-library/react';

import Rating from '../Rating';

describe('Rating component', () => {
  const props = {
    rating: { timeValues: [1000, 1000], lastValue: 1 },
  };

  test('renders correctly', () => {
    const { container } = render(<Rating {...props} />);
    expect(container.firstChild).toMatchSnapshot();
    expect(screen.getAllByText(/0:01\.0/)).toHaveLength(2);
    //screen.debug();
  });
});
