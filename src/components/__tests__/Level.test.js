import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';

import Level from '../Level';

describe('Level component', () => {
  const props = {
    tableLevels: [4, 5],
    currentLevel: 4,
    onClick: jest.fn(),
  };

  test('renders correctly', () => {
    const { container } = render(<Level {...props} />);
    expect(container.firstChild).toMatchSnapshot();
    expect(container.getElementsByClassName('slick-slider').length).toBe(1);
    expect(screen.getByText('4X4')).toBeInTheDocument();
    expect(screen.getByText('5X5')).toBeInTheDocument();
  });

  test('calls onClick prop when clicked', () => {
    render(<Level {...props} />);
    fireEvent.click(screen.getByText('4X4'));
    fireEvent.click(screen.getByText('5X5'));
    expect(props.onClick).toHaveBeenCalledTimes(2);
  });
});
