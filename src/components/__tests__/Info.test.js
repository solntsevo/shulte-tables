import React from 'react';
import { render, screen } from '../../jest/test-utils';

import Info from '../Info';

describe('Info component', () => {
  test('renders correctly with initial state', () => {
    const { container } = render(<Info />, {
      initialState: { nextValue: 13, error: false },
    });
    expect(container.firstChild).toMatchSnapshot();
    expect(screen.getByText(/13/)).toBeInTheDocument();
    //screen.debug();
  });
});
