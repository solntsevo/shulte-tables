import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import Cell from '../Cell';

describe('Cell component', () => {
  const props = {
    value: 1,
    level: 4,
    active: false,
    fontSize: 24,
    onClick: jest.fn(),
  };

  test('renders correctly', () => {
    const { container } = render(<Cell {...props} />);
    expect(container.firstChild).toMatchSnapshot();
    expect(screen.getByText('1')).toBeInTheDocument();
    //screen.debug();
  });

  test('calls onClick prop when clicked', () => {
    render(<Cell {...props} />);
    //userEvent.click(screen.getByText(/1/));
    userEvent.click(screen.getByTestId(/1/));
    expect(props.onClick).toHaveBeenCalledTimes(1);
    expect(props.onClick).toHaveBeenCalledWith(1);
  });
});
