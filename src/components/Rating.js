import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ms from 'pretty-ms';

const RatingHeader = styled.div`
  padding: 10px 0;
  text-transform: uppercase;
  text-align: center;
  font-size: 18px;
  font-weight: 600;
  background: #1c3c40;
  color: #fff;
  @media (max-width: 780px) and (orientation: portrait) {
    padding: 6px 0;
    font-size: 14px;
  }
  @media (max-width: 1200px) {
    font-size: 16px;
  }
`;
const RatingField = styled.div`
  margin: 1px;
  padding: 10px 0;
  text-align: center;
  font-size: 18px;
  font-weight: 600;
  color: ${(props) => (props.last ? '#1B937D' : '#1C3C40')};
  background: #fff;
  opacity: ${(props) => (props.last ? 0.75 : 1)};
  position: relative;
  & > span {
    position: absolute;
    left: 0;
    margin-left: 14px;
  }
  @media (max-width: 1200px) {
    font-size: 16px;
  }
  @media (max-width: 780px) and (orientation: portrait) {
    padding: 6px 0;
    font-size: 14px;
  }
`;

export default function ComponentRating({ rating }) {
  const values = rating.timeValues.map((value, index) => {
    return (
      <RatingField
        key={index}
        last={value === rating.lastValue ? true : false}
      >
        <span>{index + 1}.</span>
        {ms(value, { colonNotation: true, keepDecimalsOnWholeSeconds: true })}
      </RatingField>
    );
  });

  return (
    <div>
      <RatingHeader>Рейтинг</RatingHeader>
      {values}
    </div>
  );
}

ComponentRating.propTypes = {
  rating: PropTypes.shape({
    timeValues: PropTypes.array,
    lastValue: PropTypes.number,
  }),
};
