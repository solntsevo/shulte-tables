import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const ShulteSlider = styled(Slider)`
  width: 93%;
  margin: 3px auto 0;
  line-height: 40px;
  vertical-align: middle;
`;

const ShulteLevel = styled.button`
  text-align: center;
  font-size: 18px;
  font-weight: 600;
  letter-spacing: 6px;
  background: transparent;
  color: ${(props) => (props.active === 'true' ? '#D76001' : '#FFF')};
  border: none;
  outline: none;
  opacity: 0.75;
  cursor: pointer;

  &:hover {
    opacity: 1;
  }

  @media (max-width: 1200px) {
    font-size: 16px;
  }
`;

export default function ComponentLevel(props) {
  const { tableLevels, currentLevel, onClick } = props;

  const levels = tableLevels.map((level) => {
    return (
      <ShulteLevel
        key={level}
        active={currentLevel === level ? 'true' : 'false'}
        onClick={onClick.bind(null, level)}
      >
        {level}X{level}
      </ShulteLevel>
    );
  });

  return (
    <ShulteSlider
      speed={500}
      slidesToShow={4}
      slidesToScroll={1}
      infinite={false}
    >
      {levels}
    </ShulteSlider>
  );
}

ComponentLevel.propTypes = {
  tableLevels: PropTypes.array,
  currentLevel: PropTypes.number,
  onClick: PropTypes.func,
};
