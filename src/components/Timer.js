import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import ms from "pretty-ms";

import { ReactComponent as TimerLogo } from "../assets/images/timer.svg";

const TimerButton = styled.button`
  width: 90px;
  padding: 6px 16px;
  font-style: inherit;
  font-size: 18px;
  font-weight: bold;
  background: #fff;
  color: #376461;
  border: 1px solid #fff;
  border-radius: 20px;
  cursor: pointer;
  opacity: 0.75;
  outline: none;
  transition: 0.3s;
  &:hover {
    opacity: 1;
  }
  &:disabled {
    cursor: default;
  }
  &:disabled:hover {
    opacity: 0.75;
  }

  @media (max-width: 1200px) {
    font-size: 16px;
  }

  @media (max-width: 780px) {
    font-size: 14px;
  }
`;

const TimerValue = styled.span`
  flex: 1;
  margin-left: 10px;
  font-size: 28px;
  font-weight: medium;
  color: #fff;
  @media (max-width: 1200px) {
    font-size: 26px;
  }
  @media (max-width: 780px) {
    font-size: 28px;
  }
`;

const TimerFlex = styled.div`
  display: flex;
  align-items: center;
`;

export default function Timer({ enabled, time, onClick }) {
  return (
    <TimerFlex>
      <TimerLogo />
      <TimerValue>
        {ms(time, {
          colonNotation: true,
          keepDecimalsOnWholeSeconds: true,
        })}
      </TimerValue>
      <TimerButton onClick={onClick} disabled={enabled ? false : true}>
        СТОП
      </TimerButton>
    </TimerFlex>
  );
}

Timer.propTypes = {
  enabled: PropTypes.bool,
  time: PropTypes.number,
  onClick: PropTypes.func,
};
