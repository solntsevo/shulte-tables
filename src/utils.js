// eslint-disable-next-line no-extend-native
Object.defineProperty(Array.prototype, 'shuffle', {
  value: function () {
    for (let i = this.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [this[i], this[j]] = [this[j], this[i]];
    }
    return this;
  },
});

function createValues(level, shuffle=true) {
  const values = [];
  for (let i = 1; i <= level * level; i++) {
    values.push(i);
  }
  shuffle && values.shuffle();
  return values;
}

export { createValues };
