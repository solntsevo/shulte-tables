import React from 'react';
import { connect } from 'react-redux';

import ComponentRating from '../components/Rating';

export function ContainerRating({ level, statistics }) {
  const rating = statistics[level];
  return (
    <ComponentRating rating={rating} />
  );
}

function mapStateToProps(state) {
  return {
    level: state.currentLevel,
    statistics: state.statistics,
  };
}

export default connect(mapStateToProps)(ContainerRating);
