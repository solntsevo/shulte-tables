import React, { useMemo, useEffect, useRef, useState } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import Cell from '../components/Cell';
import { setCurrentValue } from '../redux/actions';

const TableContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const ContainerTable = (props) => {
  const { game, currentLevel, tableData, setCurrentValue } = props;
  const [tableWidth, setTableWidth] = useState(0);
  const tableRef = useRef();

  const fontSize = useMemo(() => (tableWidth / currentLevel) * 0.6, [
    currentLevel,
    tableWidth,
  ]);

  const handleClick = (chosenValue) => {
    setCurrentValue(chosenValue);
  };

  useEffect(() => {
    const width = tableRef.current.offsetWidth;
    setTableWidth(width);
  }, [currentLevel]);

  return (
    <TableContainer ref={tableRef}>
      {tableData.map((value) => {
        return (
          <Cell
            key={value}
            value={value}
            level={currentLevel}
            active={game}
            fontSize={fontSize}
            onClick={handleClick}
          />
        );
      })}
    </TableContainer>
  );
};

function mapStateToProps(state) {
  return {
    game: state.gameInProgress,
    tableData: state.tableData,
    currentLevel: state.currentLevel,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setCurrentValue: (number) => dispatch(setCurrentValue(number)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContainerTable);
