import React, { useState, useEffect, useCallback } from 'react';
import { connect } from 'react-redux';

import ComponentTimer from '../components/Timer';
import { setTimerValue, setGame } from '../redux/actions';

export function ContainerTimer({ enabled, setTimerValue, setGame }) {
  const [intervalId, setIntervalId] = useState(null);
  const [counter, setCounter] = useState({ start: 0, value: 0 });

  const startCounter = () => {
    setCounter({ ...counter, start: Date.now() });
    const id = setInterval(() => {
      setCounter((counter) => ({
        ...counter,
        value: Date.now() - counter.start,
      }));
    }, 100);
    setIntervalId(id);
  };

  const stopCounter = useCallback(() => {
    clearInterval(intervalId);
    setIntervalId(null);
    setCounter({ start: 0, value: 0 });
  }, [intervalId]);

  useEffect(() => {
    if (enabled === true) {
      startCounter();
    }
    else if (enabled === false) {
      setTimerValue(counter.value);
      stopCounter();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [enabled]);

  const handleClick = () => {
    setGame(false);
  };

  return (
    <ComponentTimer
      enabled={enabled}
      time={counter.value}
      onClick={handleClick}
    />
  );
}

function mapStateToProps(state) {
  return {
    enabled: state.gameInProgress,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setGame: (data) => dispatch(setGame(data)),
    setTimerValue: (data) => dispatch(setTimerValue(data)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContainerTimer);
