import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import ComponentLevel from '../components/Level';
import { setLevel } from '../redux/actions';

export function ContainerLevel(props) {
  const { tableLevels, currentLevel, setLevel } = props;

  const handleClick = (size) => {
    setLevel(size);
  };

  return (
    <ComponentLevel
      tableLevels={tableLevels}
      currentLevel={currentLevel}
      onClick={handleClick}
    />
  );
}

function mapStateToProps(state) {
  return {
    tableLevels: state.tableLevels,
    currentLevel: state.currentLevel,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setLevel: (size) => dispatch(setLevel(size)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContainerLevel);

ContainerLevel.propTypes = {
  tableLevels: PropTypes.array,
  currentLevel: PropTypes.number,
  setLevel: PropTypes.func,
};
