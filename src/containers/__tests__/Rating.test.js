import React from 'react';
import { render, screen } from '@testing-library/react';
import { ContainerRating } from '../Rating';

describe('Rating container', () => {
  const props = {
    level: 2,
    statistics: {
      2: { timeValues: [2000, 2000], lastValue: 1 },
    },
  };

  test('renders correctly with initial state', () => {
    const { container } = render(<ContainerRating {...props} />);
    expect(container.firstChild).toMatchSnapshot();
    expect(screen.getAllByText(/0:02\.0/)).toHaveLength(2);
    //screen.debug();
  });
});
