import React from 'react';
import configureMockStore from 'redux-mock-store';
import { render, screen, fireEvent } from '@testing-library/react';

import { ContainerTimer } from '../Timer';
import { setGame, setTimerValue } from '../../redux/actions';

describe('Timer container', () => {
  const state = {
    enabled: null,
  };
  const mockStore = configureMockStore();
  const store = mockStore(state);
  const props = {
    ...store.getState(),
    setTimerValue: jest.fn((data) => store.dispatch(setTimerValue(data))),
    setGame: jest.fn((data) => store.dispatch(setGame(data))),
  };

  afterEach(() => {
    props.setTimerValue.mockClear();
    props.setGame.mockClear();
    store.clearActions();
  });

  test('renders correctly with initial state', () => {
    const { container } = render(<ContainerTimer {...props} />);

    expect(container.firstChild).toMatchSnapshot();
    expect(screen.getByText(/0:00\.0/)).toBeInTheDocument();
    expect(screen.getByText(/стоп/i)).toBeInTheDocument();
    //screen.debug();
  });

  test('works correctly when game enabled', async () => {
    const newProps = { ...props, enabled: true };
    const { rerender } = render(<ContainerTimer {...newProps} />);

    expect(await screen.findByText(/0:00\.1/)).toBeInTheDocument();

    fireEvent.click(screen.getByText(/СТОП/));
    expect(newProps.setGame).toHaveBeenCalledTimes(1);

    newProps.enabled = false;
    rerender(<ContainerTimer {...newProps} />);
    expect(await screen.findByText(/0:00\.0/)).toBeInTheDocument();
  });

  test('dispatches action SET_GAME', () => {
    const newProps = { ...props, enabled: true };
    render(<ContainerTimer {...newProps} />);

    fireEvent.click(screen.getByText(/СТОП/));

    const actions = store.getActions();
    expect(actions).toEqual([
      {
        type: 'SET_GAME',
        status: false,
      },
    ]);
  });

  test('dispatches action SET_GAME', () => {
    const newProps = { ...props, enabled: false };
    render(<ContainerTimer {...newProps} />);

    const actions = store.getActions();
    expect(actions).toEqual([
      {
        type: 'SET_TIMER_VALUE',
        value: 0,
      },
    ]);
  });
});
