import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import configureMockStore from 'redux-mock-store';

import { ContainerTable } from '../Table';
import { setCurrentValue } from '../../redux/actions';

describe('Table container', () => {
  const state = {
    game: null,
    currentLevel: 2,
    tableData: [1, 4, 3, 2],
  };

  const mockStore = configureMockStore();
  const store = mockStore(state);
  const props = {
    ...store.getState(),
    setCurrentValue: jest.fn((data) => store.dispatch(setCurrentValue(data))),
  };

  afterEach(() => {
    props.setCurrentValue.mockClear();
    store.clearActions();
  });

  test('renders correctly with initial state', () => {
    const { container } = render(<ContainerTable {...props} />);
    expect(container.firstChild).toMatchSnapshot();

    expect(screen.getByText(/1/)).toBeInTheDocument();
    expect(screen.getByText(/2/)).toBeInTheDocument();
    expect(screen.getByText(/3/)).toBeInTheDocument();
    expect(screen.getByText(/4/)).toBeInTheDocument();
    //screen.debug();
  });

  test('works correctly', () => {
    render(<ContainerTable {...props} />);
    userEvent.click(screen.getByText(/1/));
    userEvent.click(screen.getByText(/2/));
    userEvent.click(screen.getByText(/3/));
    userEvent.click(screen.getByText(/4/));
    expect(props.setCurrentValue).toHaveBeenCalledTimes(4);
  });

  test('dispatches action SET_CURRENT_VALUE', () => {
    const actions = store.getActions();
    render(<ContainerTable {...props} />);

    userEvent.click(screen.getByTestId('1'));
    expect(actions).toEqual([
      {
        type: 'SET_CURRENT_VALUE',
        value: 1,
      },
    ]);
  });
});
