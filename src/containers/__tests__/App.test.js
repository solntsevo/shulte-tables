import React from 'react';
import { Provider } from 'react-redux';
import { render, screen } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';

import { initialState } from '../../redux/reducers';
import { setStatistics } from '../../redux/actions';
import { createValues } from '../../utils';
import { App } from '../App';

describe('App', () => {
  const state = {
    ...initialState,
    tableData: createValues(initialState.currentLevel, false),
  };
  const mockStore = configureMockStore();
  const store = mockStore(state);
  const props = {
    setStatistics: jest.fn((data) => store.dispatch(setStatistics(data))),
  };

  test('renders correctly with initial state', () => {
    const { container } = render(
      <Provider store={store}>
        <App {...props} />
      </Provider>
    );
    expect(container.firstChild).toMatchSnapshot();
    expect(props.setStatistics).toHaveBeenCalledTimes(0);
    //screen.debug();
  });

  test('works correctly and dispatches action SET_STATISTICS', () => {
    const actions = store.getActions();
    const statistics = {};
    localStorage.setItem('shulteStatistics', JSON.stringify(statistics));
    render(
      <Provider store={store}>
        <App {...props} />
      </Provider>
    );

    expect(props.setStatistics).toHaveBeenCalledTimes(1);
    expect(actions).toEqual([{
      type: 'SET_STATISTICS',
      statistics,
    }])
  });
});
