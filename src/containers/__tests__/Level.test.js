import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import configureMockStore from 'redux-mock-store';

import { ContainerLevel } from '../Level';
import { setLevel } from '../../redux/actions';

describe('Level container', () => {
  const state = {
    tableLevels: [4, 5],
    currentLevel: 4,
  };
  const mockStore = configureMockStore();
  const store = mockStore(state);
  const props = {
    ...store.getState(),
    setLevel: jest.fn((data) => store.dispatch(setLevel(data))),
  };

  afterEach(() => {
    props.setLevel.mockClear();
    store.clearActions();
  });

  test('renders correctly with initial state', () => {
    const { container } = render(<ContainerLevel {...props} />);
    expect(container.firstChild).toMatchSnapshot();

    const slider = container.getElementsByClassName('slick-slider')[0];
    expect(slider).toContainElement(screen.getByText('4X4'));
    expect(slider).toContainElement(screen.getByText('5X5'));
    //screen.debug();
  });

  test('calls onClick prop when clicked', () => {
    render(<ContainerLevel {...props} />);
    fireEvent.click(screen.getByText('4X4'));
    fireEvent.click(screen.getByText('5X5'));
    expect(props.setLevel).toHaveBeenCalledTimes(2);
  });

  test('dispatches action SET_LEVEL', () => {
    const actions = store.getActions();
    render(<ContainerLevel {...props} />);

    fireEvent.click(screen.getByText('4X4'));
    expect(actions).toEqual([
      {
        type: 'SET_LEVEL',
        number: 4,
      },
    ]);
  });
});
