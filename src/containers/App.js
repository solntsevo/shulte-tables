import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import styled, { createGlobalStyle } from 'styled-components';

import TimerContainer from '../containers/Timer';
import LevelContainer from '../containers/Level';
import TableContainer from '../containers/Table';
import RatingContainer from '../containers/Rating';
import InfoComponent from '../components/Info';
import { setStatistics } from '../redux/actions';

const GlobalStyle = createGlobalStyle`
    html {
        box-sizing: border-box;
        font-family: 'Roboto', sans-serif;
    }
    body {
        min-height: 100vh;
        width: 100%;
        background: #376461;
    }
    *, *:before, *:after {
        box-sizing: inherit;
    }
`;

const Wrapper = styled.div`
  max-width: 1170px;
  width: 100%;
  padding: 0 15px;
  margin: 0 auto;
  display: grid;
  grid-template-areas:
    'header header'
    'level  timer'
    'table  rating'
    'table  info';
  grid-template-rows: 100px 50px 1fr 1fr;
  grid-template-columns: 65% 35%;

  @media (max-width: 1600px) {
    grid-template-rows: 80px 50px 1fr 1fr;
    grid-template-columns: 55% 45%;
  }

  @media (max-width: 1200px) {
    max-width: 970px;
    grid-template-areas:
      'level header'
      'table info'
      'table timer'
      'table rating';
    grid-template-rows: 120px auto;
    grid-template-columns: 55% 45%;
  }

  @media (max-width: 1024px) and (orientation: landscape) {
    max-width: 992px;
    grid-template-areas:
      'level header'
      'table info'
      'table timer'
      'table rating';
    grid-template-rows: 100px auto;
    grid-template-columns: 55% 45%;
  }

  @media (max-width: 1024px) and (orientation: portrait) {
    max-width: 684px;
    grid-template-areas:
      'header header'
      'timer  timer'
      'level  level'
      'table  table'
      'rating info';
    grid-template-rows: 60px 60px auto;
    grid-template-columns: 50% 50%;
  }

  @media (max-width: 768px) and (orientation: portrait) {
    max-width: 568px;
    grid-template-areas:
      'header header'
      'timer  info'
      'level  level'
      'table  table'
      'rating rating';
    grid-template-rows: 60px 60px 60px auto;
    grid-template-columns: 65% 35%;
  }
`;

const Header = styled.header`
  grid-area: header;
  align-self: center;
`;

const Title = styled.h1`
  margin: 0;
  font-family: 'Lobster', cursive;
  font-size: 40px;
  font-weight: 400;
  letter-spacing: 5px;
  text-align: center;
  color: #fff;
  @media (max-width: 1200px) {
    font-size: 30px;
    margin-left: 30%;
  }
  @media (max-width: 1024px) {
    margin: 0;
  }
`;

const Level = styled.div`
  grid-area: level;
  min-width: 0;
  @media (max-width: 1200px) {
    align-self: center;
  }
`;

const Table = styled.main`
  grid-area: table;
`;

const Timer = styled.div`
  grid-area: timer;
  margin-left: 25%;
  @media (max-width: 1600px) {
    margin-left: 35%;
  }
  @media (max-width: 1200px) {
    margin-left: 30%;
    align-self: end;
  }
  @media (max-width: 1024px) {
    margin: 0;
    align-self: center;
  }

  @media (max-width: 1024px) and (orientation: landscape) {
    margin: 20px 0 0 8%;
  }
`;

const Rating = styled.div`
  grid-area: rating;
  margin-left: 25%;
  @media (max-width: 1600px) {
    margin-left: 35%;
  }
  @media (max-width: 1200px) {
    margin-left: 30%;
    align-self: end;
  }
  @media (max-width: 1024px) and (orientation: portrait) {
    margin: 50px 0 0 0;
  }

  @media (max-width: 1024px) and (orientation: landscape) {
    margin: 0 0 0 8%;
  }
`;
const Info = styled.div`
  grid-area: info;
  margin-left: 25%;
  text-align: center;
  @media (max-width: 1600px) {
    margin-left: 35%;
    align-self: center;
  }
  @media (max-width: 1200px) {
    margin-left: 30%;
    align-self: center;
  }
  @media (max-width: 1024px) {
    margin: 50px 0 0 0;
  }
  @media (max-width: 1024px) and (orientation: landscape) {
    margin: 5% 0 0 8%;
  }
  @media (max-width: 768px) {
    margin: -5px 0 0 0;
    justify-self: end;
  }
`;

export function App({ setStatistics }) {
  useEffect(() => {
    const localStatistics = localStorage.getItem('shulteStatistics');
    if (localStatistics) {
      setStatistics(JSON.parse(localStatistics));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <React.Fragment>
      <GlobalStyle />
      <Wrapper>
        <Header>
          <Title>Таблицы Шульте</Title>
        </Header>
        <Level>
          <LevelContainer />
        </Level>
        <Timer>
          <TimerContainer />
        </Timer>
        <Table id="shulteTable">
          <TableContainer />
        </Table>
        <Rating>
          <RatingContainer />
        </Rating>
        <Info>
          <InfoComponent />
        </Info>
      </Wrapper>
    </React.Fragment>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    setStatistics: (statistics) => dispatch(setStatistics(statistics)),
  };
}

export default connect(null, mapDispatchToProps)(App);
