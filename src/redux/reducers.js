import { createValues } from "../utils";

export const initialState = {
  gameInProgress: null,
  winner: false,
  tableLevels: [4, 5, 6, 7, 8, 9],
  tableData: createValues(4),
  currentLevel: 4,
  currentValue: null,
  nextValue: 1,
  timerInAction: false,
  timerLastValue: 0,
  error: false,
  statistics: {
    4: { timeValues: [0, 0, 0, 0, 0], lastValue: null },
    5: { timeValues: [0, 0, 0, 0, 0], lastValue: null },
    6: { timeValues: [0, 0, 0, 0, 0], lastValue: null },
    7: { timeValues: [0, 0, 0, 0, 0], lastValue: null },
    8: { timeValues: [0, 0, 0, 0, 0], lastValue: null },
    9: { timeValues: [0, 0, 0, 0, 0], lastValue: null },
  },
};

function rootReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_GAME":
      return Object.assign({}, state, { gameInProgress: action.status });
    case "SET_TIMER_ACTION":
      return Object.assign({}, state, { timerInAction: action.payload });
    case "SET_TIMER_VALUE":
      return Object.assign({}, state, { timerLastValue: action.value });
    case "SET_LEVEL":
      return Object.assign({}, state, { currentLevel: action.number });
    case "SET_TABLE":
      return Object.assign({}, state, { tableData: createValues(action.size) });
    case "SET_CURRENT_VALUE":
      return Object.assign({}, state, { currentValue: action.value });
    case "SET_NEXT_VALUE":
      return Object.assign({}, state, { nextValue: action.value });
    case "SET_ERROR":
      return Object.assign({}, state, { error: action.status });
    case "SET_STATISTICS":
      return Object.assign({}, state, { statistics: action.statistics });
    case "SET_WINNER":
      return Object.assign({}, state, { winner: action.status });
    default:
      return state;
  }
}

export default rootReducer;
