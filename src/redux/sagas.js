import { put, takeEvery, select } from 'redux-saga/effects';
import {
  setGame,
  setWinner,
  setNextValue,
  setError,
  setTable,
  setStatistics,
} from './actions';

const START_VALUE = 1;

function* sagaProcessGame(action) {
  const state = yield select();

  if (!action.status) {
    yield put(setNextValue(START_VALUE));
    yield put(setTable(state.currentLevel));
    if (state.error) {
      yield put(setError(false));
    }
  }

  if (action.status && state.winner) {
    yield put(setWinner(false));
  }
}

export function* sagaProcessLevel(action) {
  const state = yield select();

  if (state.gameInProgress) {
    yield put(setGame(false));
  } else {
    yield put(setTable(action.number));
  }
}

function* sagaProcessCurrentValue(action) {
  const state = yield select();
  const maxValue = Math.pow(state.currentLevel, 2);

  if (!state.gameInProgress) {
    if (action.value === 1) {
      yield put(setGame(true));
      yield put(setNextValue(action.value + 1));
    }
  } else {
    if (state.nextValue !== action.value) {
      yield put(setError(true));
    }
    else {
      if (state.error) {
        yield put(setError(false));
      }

      if (action.value < maxValue) {
        yield put(setNextValue(action.value + 1));
      } else {
        yield put(setGame(false));
        yield put(setWinner(true));
      }
    }
  }
}

function* sagaProcessTimerValue(action) {
  const state = yield select();

  if (state.winner) {
    const statistics = { ...state.statistics };
    const level = state.currentLevel;
    const time = action.value;
    const values = statistics[level].timeValues;

    values.pop();
    values.unshift(time);
    // eslint-disable-next-line array-callback-return
    values.sort(function (a, b) {
      if (a > b && b !== 0) return 1;
      if (a === b) return 0;
      if (a < b && a !== 0) return -1;
    });
    statistics[level].lastValue = time;
    localStorage.setItem('shulteStatistics', JSON.stringify(statistics));
    yield put(setStatistics(statistics));
  }
}

export default function* rootSaga() {
  yield takeEvery('SET_GAME', sagaProcessGame);
  yield takeEvery('SET_LEVEL', sagaProcessLevel);
  yield takeEvery('SET_CURRENT_VALUE', sagaProcessCurrentValue);
  yield takeEvery('SET_TIMER_VALUE', sagaProcessTimerValue);
}
