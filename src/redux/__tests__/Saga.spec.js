import SagaTester from 'redux-saga-tester';
import rootSaga from '../sagas';
import { initialState } from '../reducers';
import * as a from '../actions';

describe('Saga', () => {
  let sagaTester;
  let defaultReducer = (state, action) => {
    return Object.assign({}, state, action.payload);
  }
  const START_VALUE = 1;
  const RANDOM_VALUE = 3;
  const MAX_VALUE = Math.pow(initialState.currentLevel, 2);
  const TIMER_VALUE = 1000;

  beforeEach(() => {
    sagaTester = new SagaTester({ initialState, reducers: defaultReducer });
    sagaTester.start(rootSaga);
  });

  test('works correctly with action SET_GAME', async () => {
    // Game in progress
    sagaTester.dispatch(a.setGame(false));
    let successSetNextValue = await sagaTester.waitFor('SET_NEXT_VALUE');
    expect(successSetNextValue).toEqual(a.setNextValue(START_VALUE));
    let successSetTable = await sagaTester.waitFor('SET_TABLE');
    expect(successSetTable).toEqual(a.setTable(initialState.currentLevel));
    expect(sagaTester.wasCalled('SET_ERROR')).toBeFalsy();

    sagaTester.updateState({ ...initialState, error: true  })
    sagaTester.dispatch(a.setGame(false));
    expect(sagaTester.wasCalled('SET_ERROR')).toBeTruthy();

    // No game
    sagaTester.dispatch(a.setGame(true));
    expect(sagaTester.wasCalled('SET_WINNER')).toBeFalsy();

    sagaTester.updateState({ ...initialState, winner: true  })
    sagaTester.dispatch(a.setGame(true));
    expect(sagaTester.wasCalled('SET_WINNER')).toBeTruthy();
  });


  test('works correctly with action SET_LEVEL', async () => {
    // No game
    sagaTester.dispatch(a.setLevel(initialState.currentLevel));
    let successSetTable = await sagaTester.waitFor('SET_TABLE');
    expect(successSetTable).toEqual(a.setTable(initialState.currentLevel));

    // Game in progress
    sagaTester.updateState({ ...initialState, gameInProgress: true  })
    sagaTester.dispatch(a.setLevel(initialState.currentLevel));
    const successSetGame = await sagaTester.waitFor('SET_GAME');
    expect(successSetGame).toEqual(a.setGame(false));
  });

  test('works correctly with action SET_CURRENT_VALUE', async () => {
    // No game with no clicked '1'
    sagaTester.dispatch(a.setCurrentValue(RANDOM_VALUE));
    expect(sagaTester.wasCalled('SET_GAME')).toBeFalsy();
    expect(sagaTester.wasCalled('SET_NEXT_VALUE')).toBeFalsy();

    // No game with clicked '1'
    sagaTester.dispatch(a.setCurrentValue(START_VALUE));
    expect(sagaTester.wasCalled('SET_GAME')).toBeTruthy();
    let successSetNextValue = await sagaTester.waitFor('SET_NEXT_VALUE');
    expect(successSetNextValue).toEqual(a.setNextValue(START_VALUE + 1));

    // Game in progress with no clicked next value
    sagaTester.updateState({ ...initialState, gameInProgress: true, nextValue: RANDOM_VALUE });
    sagaTester.dispatch(a.setCurrentValue(START_VALUE));
    let successSetError = await sagaTester.waitFor('SET_ERROR');
    expect(successSetError).toEqual(a.setError(true));

    // Game in progress with clicked next value
    sagaTester.dispatch(a.setCurrentValue(RANDOM_VALUE));
    expect(sagaTester.getLatestCalledAction()).toEqual(a.setNextValue(RANDOM_VALUE + 1));

    // Game in progress with clicked last value
    sagaTester.reset(true)
    sagaTester.updateState({ ...initialState, gameInProgress: true, nextValue: MAX_VALUE });
    sagaTester.dispatch(a.setCurrentValue(MAX_VALUE));
    const successSetGame = await sagaTester.waitFor('SET_GAME');
    expect(successSetGame).toEqual(a.setGame(false));
    const successSetWinner = await sagaTester.waitFor('SET_WINNER');
    expect(successSetWinner).toEqual(a.setWinner(true));
  });

  test('works correctly with action SET_TIMER_VALUE', async () => {
    // End game with no winner
    sagaTester.dispatch(a.setTimerValue(TIMER_VALUE));
    expect(sagaTester.getCalledActions()).toHaveLength(1);
    expect(sagaTester.wasCalled('SET_STATISTICS')).toBeFalsy();

    // End game with winner
    sagaTester.updateState({ ...initialState, winner: true });
    sagaTester.dispatch(a.setTimerValue(TIMER_VALUE));
    expect(sagaTester.wasCalled('SET_STATISTICS')).toBeTruthy();
  })
});
