function setGame(data) {
  return {
    type: 'SET_GAME',
    status: data,
  };
}

function setWinner(data) {
  return {
    type: 'SET_WINNER',
    status: data,
  };
}

function setLevel(data) {
  return {
    type: 'SET_LEVEL',
    number: data,
  };
}

function setTable(data) {
  return {
    type: 'SET_TABLE',
    size: data,
  };
}

function setCurrentValue(data) {
  return {
    type: 'SET_CURRENT_VALUE',
    value: data,
  };
}

function setNextValue(data) {
  return {
    type: 'SET_NEXT_VALUE',
    value: data,
  };
}

function setError(data) {
  return {
    type: 'SET_ERROR',
    status: data,
  };
}

function setTimerValue(data) {
  return {
    type: 'SET_TIMER_VALUE',
    value: data,
  };
}

function setStatistics(data) {
  return {
    type: 'SET_STATISTICS',
    statistics: data,
  };
}

export {
  setGame,
  setWinner,
  setLevel,
  setTable,
  setCurrentValue,
  setNextValue,
  setError,
  setStatistics,
  setTimerValue,
};
